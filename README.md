# Web Service | Helm Chart | Twuni

This Helm chart deploys a standard 12-Factor web service.

It makes the following assumptions about your app:

 * The app is built to be deployed as a Docker image
 * The app is configured via environment variables
 * The app is listening on the standard HTTPS port (443)
 * The app emits its logs to standard output
 * The app can be deployed independently as a single container without any tightly coupled "sidecar" containers
